<html>
<?php include("include/menu.php"); ?>
<?php
echo "Kiểu giá trị <br>";
?>

<?php 
$bien_int = 12345;
$bien_int_khac = -12345 + 12345;
echo "Đây là kiểu int  $bien_int --- :  -12345 + 12345 = $bien_int_khac  <br>";

?>

<?php
   $bien_double_1 = 25.248;
   $bien_double_2 = 19.266;
   $ket_qua = $bien_double_1 + $bien_double_2;
   
   print("Đây là kiểu double : -- $bien_double_1 + $bien_double_2 = $ket_qua <br>");
?>



<?php
echo "Đây là kiểu String:  <br>";
   $bien_1 = "my name is Diamond";
   print("bien_1 = $bien_1");
   $bien_2 = '$bien_1 sẽ không được in! : không lấy giá trị của biến khi dùng nháy đơn';
   print("$bien_2 <br>");
   echo "độ dài của chuỗi bien_1 :", strlen($bien_1),"<br>" ;
   echo "vị trí chữ Diamond trong bien_1 :", strpos($bien_1,"Diamond"),"<br>" ;
   $bien_2 = "$bien_1 sẽ được in! : Lấy giá trị của biến khi dùng nháy kép";
   print( " <br>");
?>
<?php
echo "Đây là kiểu array:  <br>";
$array = array( 1, 2, 3, 4, 5);
   
  foreach($array as $value){
    echo "Giá trị phần tử mảng là $value <br />";
  }

?>
</html>