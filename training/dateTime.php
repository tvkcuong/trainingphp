<?php include("include/menu.php"); ?>
<?php
   echo "Bây giờ là: ";
   echo date("l jS \of F Y H:i:s") . "<br>";
   echo "Bây giờ là: ";
   echo date("l d - m - Y h:i:s A") . "<br>";


   // hiển thị October 3, 1975 là vào Friday
   echo "Oct 3,1975 was on a ".date("l", mktime(0,0,0,10,3,1975)) . "<br>";

   // sử dụng một hằng số trong tham số format
   echo date(DATE_RFC822) . "<br>";

   // hiển thị date time dưới dạng giống như: 1975-10-03T00:00:00+00:00
   echo date(DATE_ATOM,mktime(0,0,0,10,3,1975));
?>