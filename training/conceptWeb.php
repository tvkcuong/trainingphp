<?php 
function getbrowser()
{
  $u_agent = $_SERVER['HTTP_USER_AGENT'];
  $bname = "UnKnown";
  $platform = "Unknown";
  $version = "Unknown";

  //check platForm
  if(preg_match('/linux/i', $u_agent)) {
    $platform = 'linux'; 
  }elseif(preg_match('/windows|win32/i', $u_agent)){
    $platform = "windows";

  }elseif(preg_match('/macintosh|mac os x/i', $u_agent)){
    $platform ="mac";
  }
  // check browser 
  if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i', $u_agent)){
  $bname = 'Mozilla Firefox';
  $browser = 'Firefox'; 
  }elseif(preg_match('/Chrome/i', $u_agent)){
      $bname = 'Google Chrome';
      $browser = 'Chrome';
  }elseif(preg_match('/Safari/i', $u_agent)){
      $bname = 'Apple Safari';
      $browser = 'Safari';
      
  }elseif(preg_match('/Opera/i', $u_agent)){
      $bname = 'Opera';
      $browser = 'Opera';
      
  }elseif(preg_match('/Netscape/i', $u_agent)){
      $bname = 'Netscape';
      $browser ='Netscape';
  }

  //get name version
  $known = array('Version',$browser,'other');
  $pattern ='#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
  if(!preg_match_all($pattern,$u_agent,$matches)){

  }
  $i = count($matches['browser']);
  if($i != 1){
      //check old version
      if(strripos($u_agent,"Version") < strripos($u_agent,$browser)){
            $version =$matches['version'][0];
      }else{
          $version = $matches['version'][1];
      }
  }else {
      $version = $matches['version'][0];
      
  }
  if($version == null || $version == ""){
      $version = "?";
  }
  return array(
      'nameBrowser'=> $u_agent,
      'name'       => $bname,
      'version' => $version,
    'platform'=>$platform, 
    'pattern' => $pattern,
     
  );
}

$ua = getbrowser();
$yourbrowser= "Your browser: " . $ua['name']. "version : " . $ua['version'] . "platform : " . $ua['platform'] . " reports: <br >" . $ua['nameBrowser'];
print_r($yourbrowser);
?>